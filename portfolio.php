<?php
    $pageID = 'portfolio';
    include('header.php');
?>

                    

    <div class="page-head">
        <h1>Portfolio</h1>
        <p>This is a sampling of personal and professional projects that I have developed and worked on.</p>
    </div>
 



    <section id="top" class="projects">

        <div id="columbusschoolofchinese" class="project-item">
            <h3 class="project-item_title">Columbus School of Chinese</h3>
    
                <figure class="project-item_figure effeckt-caption" data-effeckt-type="revolving-door-top">
                    <img class="project-item_img" src="./images/thumb-csc.jpg" alt="Website: The Columbus School of Chinese" />
                    <figcaption class="project-item_figcaption">
                      <div class="project-item_figcaption-wrap effeckt-figcaption-wrap">
                        <a href="project-detail.php" class="project-item_link">
                        <p class="project-item_caption">
                            A custom designed and developed site built on WordPress with a customized administror panel.
                            <em href="project-detail.php" class="project-item_btn">More Info</em>
                        </p>
                        </a>
                      </div>
                    </figcaption>
                </figure>
        </div>


        <div id="purpleyoga" class="project-item">
            <h3 class="project-item_title">Purple Yoga</h3>
                <figure class="project-item_figure effeckt-caption" data-effeckt-type="revolving-door-right">
                    <img class="project-item_img" src="./images/tmb-py.jpg" alt="" />
                    <figcaption class="project-item_figcaption">
                      <div class="project-item_figcaption-wrap effeckt-figcaption-wrap">
                        <p class="project-item_caption">
                            A Worpress site with a custom admin panel to make content management a breeze.
                            <a href="project-detail.php" class="project-item_link">More Info</a>                        
                        </p>
                      </div>
                    </figcaption>
                </figure>                     
        </div>


        <div id="rikshawdesign" class="project-item">
            <h3 class="project-item_title">Rikshaw Design</h3>

                <figure class="project-item_figure effeckt-caption" data-effeckt-type="revolving-door-bottom">
                    <img class="project-item_img" src="./images/tmb-rd.jpg" alt="" />
                    <figcaption class="project-item_figcaption">
                      <div class="project-item_figcaption-wrap effeckt-figcaption-wrap">
                        <p class="project-item_caption">
                            A Worpress site with a custom admin panel to make content management a breeze.
                            <a href="project-detail.php" class="project-item_link">More Info</a>                        
                        </p>
                      </div>
                    </figcaption>
                </figure>
                
        </div>


        <div id="thebusproject" class="project-item">
            <h3 class="project-item_title">The Bus Project</h3>

            <figure class="project-item_figure effeckt-caption" data-effeckt-type="revolving-door-left">
                <img class="project-item_img" src="./images/thumb-bus-project.jpg" alt="" />
                <figcaption class="project-item_figcaption">
                  <div class="project-item_figcaption-wrap effeckt-figcaption-wrap">
                        <p class="project-item_caption">
                            A Worpress site with a custom admin panel to make content management a breeze.
                            <a href="project-detail.php" class="project-item_link">More Info</a>                        
                        </p>
                  </div>
                </figcaption>
            </figure>
                 
        </div>


        <div id="socialreaderclone" class="project-item">
            <h3 class="project-item_title">Social Reader Clone</h3>

                <figure class="project-item_figure effeckt-caption" data-effeckt-type="revolving-door-top">
                    <img class="project-item_img" src="./images/thumb-bus-project.jpg" alt="" />
                    <figcaption class="project-item_figcaption">
                      <div class="project-item_figcaption-wrap effeckt-figcaption-wrap">
                        <p class="project-item_caption">
                            A Worpress site with a custom admin panel to make content management a breeze.
                            <a href="project-detail.php" class="project-item_link">More Info</a>                        
                        </p>
                      </div>
                    </figcaption>
            </figure>
             
        </div>                     

    </section>
    <!-- / PROJECTS -->
       
                
    

<?php include('footer.php'); ?>