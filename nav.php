<header class="global-header">
    <nav id="site-navigation" class="main-navigation" role="navigation">
        <a class="home-link" accesskey="1" href="./index.php">
        <img src="./images/jcampbelllogo.png" alt="Jamie Campbell: Custom Development" />
        </a>
        <h1 class="menu-toggle"><i class="fa fa-bars"></i>Menu</h1>
        <ul class="global-page-menu">
            <li class="main portfolio first">
                <a class="<?php if($pageID == 'portfolio'){echo 'current-menu-item';} ?>" href="./portfolio.php">Portfolio</a>
            </li>        
            <li class="main services">
                <a class="<?php if($pageID == 'services'){echo 'current-menu-item';} ?>" href="./services.php">Services</a>
            </li>
            <li class="main about">
                <a class="<?php if($pageID == 'about'){echo 'current-menu-item';} ?>" href="./about.php">About</a>
            </li>
            <li class="main contact">
                <a class="<?php if($pageID == 'contact'){echo 'current-menu-item';} ?>" href="./contact.php">Contact</a>
            </li>
        </ul>
    </nav>
    <div class="logomark">
        <a class="<?php if($pageID == 'home'){echo 'current-menu-item';} ?>" href="./portfolio.php">
            <img src="../images/JCam-Logomark.svg" alt="" />
        </a>
    </div>    
</header><!-- End #header -->