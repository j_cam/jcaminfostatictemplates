<?php
    $pageID = 'portfolio';
    include('header.php');
?>

                    

    <div class="page-head">
        <h1>Portfolio</h1>
    </div>
 
            <section id="top" class="projects"> 
                <section id="columbusschoolofchinese" class="project-item project-detail">                    
                    <div class="animated project-masthead">
                        <article class="project-summary">
                            <h2>The Columbus School of Chinese</h2>
                            <h4>Some details about the project</h4>
                            <p>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in risus quis tortor vulputate ornare. Nulla at mauris sagittis, cursus mi nec, varius nulla. Pellentesque enim mi, commodo vel tincidunt a, imperdiet eget urna.
                            </p>
                            <p>
                              My Role: Design, Front-end Development, Information Architecture, Mobile App Design
                            </p>
                            <a href="fdd" rel="external">columbusschoolofchinese.com</a>
                        </article>

                        <aside class="project-hero">
                            <img src="./images/slide-csc-1.jpg" alt="Website: The Columbus School of Chinese" />
                        </aside>
                    </div>

                    <article class="project-info">
                        <span class="arrow-down"></span>
                        <img class="project-supporting-image"  src="./images/slide-csc-1.jpg" alt="Website: The Columbus School of Chinese" />
                        <h4>Some details about the project</h4>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in risus quis tortor vulputate ornare. Nulla at mauris sagittis, cursus mi nec, varius nulla. Pellentesque enim mi, commodo vel tincidunt a, imperdiet eget urna.
                        </p>                        
                    </article>
                </section>        

            </section>
            <!-- / PROJECTS -->
       
                
    

<?php include('footer.php'); ?>

