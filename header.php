<!DOCTYPE html>
<html class="<?php if($pageID == 'home'){ echo $pageID;} ?>">
<head>
  	<meta charset="utf-8">
	<title>Jamie Campbell: Custom Web Development</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link href="http://einstein.franklin.edu/~campbe53/eportfolio/images/favicon.png" rel="shortcut icon">
	<meta name="google-site-verification" content="fLwPtxwI1pIQDcGKYmJ_D5ZeI9d4NzLSbEaeRM7DhE0">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta name="description" content="This is the website and portfolio of Jamie Campbell. I specialize in web design and development.">
	<meta name="keywords" content="web design, web development, email campaigns, e-commerce, web standards, freelance, Photoshop to HTML Conversion">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="./css/style.css">
	<script type="text/javascript" src="//use.typekit.net/oja7zqe.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	<?php if($pageID === 'portfolio'): ?>

	<?php endif; ?>
	</head>
	<body class="<?php echo $pageID; ?>">

		<?php require_once('nav.php'); ?>	    
	    <main class="page-content">
	    

