<?php
    $pageID = 'services';
    require_once('header.php');
?>

    <div class="page-head">
        <h1>Services</h1> 
        <p>It’s really hard to list out all the things I can do for you, but this is the short list of the most common services I offer clients &amp; partners.</p>
    </div> 

            <div class="service">
                <h3>PSD to HTML Conversion</h3>
                <img class="thumb" src="./images/service-html.jpg" alt="" />
                <p>I turn designs into clean and semantic HTML5 markup and modular CSS3 using Sass. </p>
            </div>


            <div class="service">
                <h3>WordPress Themes</h3>
                <img class="thumb" src="./images/service-wordpress.jpg" alt="" />
                <p>I can create  a custom theme from your design or convert your current site to use the power of WordPress.</p>
            </div>

            <div class="service">
                <h3>Responsive Design</h3>
                <img class="thumb" src="./images/service-responsive.jpg" alt="" />
                <p>Your users deserve a great experience no matter what their screen resolution is and responsive design does it best.</p>
            </div>



            <div class="service">
                <h3>Custom Integrations</h3>
                <img class="thumb" src="./images/service-integration.jpg" alt="" />
                <p>Web services  and integrations like analytics, email management and other your site extra functionality.</p>
            </div>
  
   
            <div class="service">
                <h3>Need Something Else?</h3>
                <img class="thumb" src="./images/service-other.jpg" alt="" />
                <p>Not everything project fits into a box. If you have a custom project get in touch with me. <a href="contact.php">Contact Me ›</a></p>
            </div>


<?php include('footer.php'); ?>