<?php
    $pageID = 'contact';
    require_once('header.php');
?>

                    
               
    <div class="page-head">
        <h1>Contact</h1>
        <p>If you have a project that needs a developer, or just have development questions in general, send me a message.</p>
    </div>

    <?php
        // Include Packages
        require_once('./application/class.Mailer.php');
        
        // Instantiate
        $mailer = new Mailer();

        // Are there post vars to evaluate?
        if(isset($_POST['submit'])){
            $mailer->validate($_POST);
        }
    ?>

    <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
        <?php 
            // if the form has been posted give user feedback   
            if($mailer->posted):
        ?>
           <div class="confirmation-message"> 
                <h2><?php echo $mailer->getConfirmHeading(); ?></h2>
                <p class="message"><em><?php echo $mailer->getConfirmMessage(); ?></em></p>
            </div>
        <?php endif; ?> 

        <?php 
            // if the submit was unsuccessful show the form again

            if(!$mailer->success): 
        ?> 
            <p class="req"><em>All fields required.</em></p>
            
            <label for="sender-email">
               Email Address
            </label>
            <input type="email" name="sender-email" id="sender-email" autocomplete="on" placeholder="Enter your email address" required>


            <label for="subject">
                Subject
            </label>
            <input type="text" name="subject" id="subject" class="subject" autocomplete="on" placeholder="What's your reason for contacting me?" required>
            
            <label for="message">
                Message
            </label>
            <textarea name="message" id="message" placeholder="What's on your mind?"></textarea>

            <input type="submit" name="submit" value="Send">

        <?php endif; ?>

    </form>        

<?php include('footer.php'); ?>
