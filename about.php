<?php
    $pageID ='about';
    include('header.php');
?>
        
        <div class="page-head">
            <h1>About</h1>
            <p>Hello World! My name is Jamie and I build custom crafted web applications.</p>
        </div>

        <section class="personal-profile">

                <h2>Personal &amp; Professional Profile</h2>
                <p>
                    My career started out with an interest in graphic design, which led me to study interactive multimedia in college. After completeing an Associates of Applied Science in Interactive Design, I created an online portfolio and took a job as a Web Designer.  While working as a Web Designer, I began teaching myself Web Development and decided to complete my Bachelors of Science in Web Development.
                </p>

                <p>
                    Today, I combine my passion for design & development to do work that I truly enjoy. My Objective is to effectively implement user interfaces using client side-side and server-side technologies. I enjoy the process of turning a Photoshop documents into a functioning web page without compromising the integrity of the original design.
                </p>

        </section><!-- END: personal-profile -->
        
        <section class="work-experience">
  
                <h2>Work Experience</h2>
                
                <div class="about-content-block">
                    <div class="block-left">
                        <h3>Innovation Engineer</h3>
                        <p>
                            <strong>Organization:</strong> Engauge
                            <br>
                            <strong>Period:</strong> 2007 - 2013
                        </p>
                    </div>
                    <div class="block-right">
                        <p>
                            I worked in many different capacities for Engauge and their clients. I went gone from doing grunt work on branded MySpace pages to playing a critical role in concepting, building, maintaining, supporting and eventually transitioning our couponing platform to a Fortune 500 company.                 
                        </p>
                    </div>
                </div><!-- END: .about-content-block -->
                
                <div class="about-content-block">
                    <div class="block-left">
                        <h3>Feelance Developer</h3>
                        <p>
                            <strong>Organization:</strong> Various<br>
                            <strong>Period:</strong> 2007 - Current
                        </p>
         
                    </div>
                    <div class="block-right">
                        <p>
                            Working on small freelance projects allows me the ability to challenge myself and experiment with emerging technologies. A recent project allowed me to work with a local artist to create an interactive component to his project that included 
                            community participation though a custom Google Maps application that I built.               
                        </p>
                    </div>
                </div><!-- END: .about-content-block -->
   
        </section><!-- END: .work-experience -->

        <section class="education">
           
                <h2>Education</h2>
                
                <div class="about-content-block">
                    <div class="block-left">
                        <h3>Franklin University</h3>
                        <p class="organization">
                            <strong>Bachelors of Science</strong>
                            <br>
                            Web Development</p>
                    </div>
                    <div class="block-right">
                        <p>
                            <strong>Coursework in: </strong>Object Oriented Programming, Data Structures & Algorithms, Database Programming, Survey of Programming Languages, Web Application Development, Human Computer Interaction, Systems Theory, Graphic Design                  
                        </p>
                    </div>
                </div><!-- END: .about-content-block -->
                
                <div class="about-content-block">
                    <div class="block-left">
                        <h3>Columbus State </h3>
                        <p class="organization">
                            <strong>Associates of Applied Science</strong>
                            <br>
                            Interactive Multimedia
                        </p>
                    </div>
                    <div class="block-right">
                        <p><strong>Coursework in: </strong>Graphic Design, Typography,Storyboarding, Digital Illustration, HTML, CSS, PHP, MySQL, Flash & ActionScript
                    </div>
                </div><!-- END: .about-content-block -->
           
        </section><!-- END: .education -->     



<?php include('footer.php'); ?>